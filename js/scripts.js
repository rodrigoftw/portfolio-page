var js = document.getElementById("js-bar");
var css = document.getElementById("css-bar");
var react = document.getElementById("react-bar");
var redux = document.getElementById("redux-bar");

var android = document.getElementById("android-bar");
var ios = document.getElementById("ios-bar");
var reactNative = document.getElementById("react-native-bar");

var nodeJS = document.getElementById("node-js-bar");
var php = document.getElementById("php-bar");

let jsMax = 100;
let cssMax = 80;
let reactMax = 90;
let reduxMax = 50;

let androidMax = 50;
let iosMax = 65;
let reactNativeMax = 80;

let nodeJsMax = 40;
let phpMax = 20;

function loadBar(element, maxWidth) {
  var i = 0;
  if (i == 0) {
    i = 1;
    var width = 1;
    var id = setInterval(frame, 10);
    function frame() {
      if (width >= maxWidth) {
        clearInterval(id);
        i = 0;
      } else {
        width++;
        element.style.width = width + "%";
      }
    }
  }
}

var elements = [js, css, react, redux, android, ios, reactNative, nodeJS, php];
var widths = [jsMax, cssMax, reactMax, reduxMax, androidMax, iosMax, reactNativeMax, nodeJsMax, phpMax];

for (let index = 0; index < elements.length; index++) {
  loadBar(elements[index], widths[index]);
}
// loadBar(js, jsMax);
// loadBar(css, cssMax);
// loadBar(react, reactMax);
// loadBar(redux, reduxMax);

// loadBar(android, androidMax);
// loadBar(ios, iosMax);
// loadBar(reactNative, reactNativeMax);

// loadBar(nodeJS, nodeJsMax);
// loadBar(php, phpMax);